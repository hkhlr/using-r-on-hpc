# Using R on HPC



## About this course

This course is part of the [HKHLR course program](https://www.hkhlr.de/en/events/courses-and-tutorials). For a detailed course description please refer to the [course website](https://www.hkhlr.de/en/events/r-hpc-systems-2023-09-04)

## Getting the course material

The course material can be obtained by cloning this repository, e.g. from within a terminal emulator of your choice, execute:
```
git clone https://git.rwth-aachen.de/hkhlr/using-r-on-hpc.git
```
Please not that you need to have Git installed. This can be done e.g. via `sudo apt-get install git` on Debian-based Linux distros, `sudo dnf install git` on Fedora-based Linux distros, or `brew install git`on macOS with [Homebrew](https://brew.sh) installed. Git for Windows can be obtained [here](https//git-scm.com/downloads).

Alternatively, you can download the repository as an archive file (`.zip`, `.tar.gz`, `tar.bz2`, or `.tar`) by clicking on the download button, immediately to the left of the 'Clone' button.

## Course material

The course content is comprised of the slides and an exercise sheet (in PDF format) as well as source code examples.
*The job submit scripts are configured to work on the MaRC3a cluster in Marburg and will probably require edits to run on other clusters!*

## Licensing

Please refer to the LICENSE file for licensing information.