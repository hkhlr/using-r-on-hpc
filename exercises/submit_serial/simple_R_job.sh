#!/bin/bash

# SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR)
# SPDX-License-Identifier: MIT

#SBATCH --job-name=simple_R_job
#SBATCH --output=simple_R_job.%j.out
#SBATCH --error=simple_R_job.%j.err
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=1G
#SBATCH --time=00:30:00

module purge
module load gnu9 openmpi4 R

export OMP_NUM_THREADS=1

Rscript growing_objects.R
