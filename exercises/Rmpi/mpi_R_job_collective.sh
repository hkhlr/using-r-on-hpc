#!/bin/bash

# SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR)
# SPDX-License-Identifier: MIT

#SBATCH --job-name=mpi_R_job_collective
#SBATCH --output=mpi_R_job_collective.%j.out
#SBATCH --error=mpi_R_job_collective.%j.err
#SBATCH --ntasks=8
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=1G
#SBATCH --time=00:30:00

module purge
module load gnu9 openmpi4 R

export OMP_NUM_THREADS=1

time mpiexec -n $SLURM_NTASKS Rscript mpi_collective.R
