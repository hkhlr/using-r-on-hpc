#!/bin/bash

# SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR)
# SPDX-License-Identifier: MIT

#SBATCH --job-name=rmpisnow_R_job
#SBATCH --output=rmpisnow_R_job.%j.out
#SBATCH --error=rmpisnow_R_job.%j.err
#SBATCH --ntasks=8
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=1G
#SBATCH --time=00:30:00

module purge
module load gnu9 openmpi4 R

export OMP_NUM_THREADS=1

mpiexec -n $SLURM_NTASKS ~/R/x86_64-pc-linux-gnu-library/4.1/snow/RMPISNOW --no-echo < rmpisnow.R
